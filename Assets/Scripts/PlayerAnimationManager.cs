﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationManager : MonoBehaviour
{
    private Animator animator;
    private InputState inputState;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        inputState = GetComponent<InputState>();
    }

    private void Update()
    {
        var running = true;

        if (inputState.absVelX > 0 && inputState.absVelY < inputState.standingTreshold)
        {
            running = false;
        }

        animator.SetBool("Running", running);
    }
}
