﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public UnityEngine.UI.Text continueText;
    public UnityEngine.UI.Text scoreText;

    private float timeElapsed = 0f;
    private float timeBest = 0f;
    private bool personalRecord = false;
    private float blinkTime = 0f;
    private bool blink;
    private bool gameStarted = false;
    private TimeManager timeManager;
    private GameObject player;
    private GameObject floor;
    private Spawner spawner;

    private string TextTimeElapsed => "TIME: " + FormatTime(timeElapsed);
    private string TextTimeBest => "BEST: " + FormatTime(timeBest);

    private void Awake()
    {
        floor = GameObject.Find("Foreground");
        spawner = GameObject.Find("Spawner").GetComponent<Spawner>();
        timeManager = GetComponent<TimeManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        var floorHeight = floor.transform.localScale.y;
        var pos = floor.transform.position;

        pos.x = 0;
        pos.y = -(Screen.height / PixelPerfectCamera.pixelsToUnits / 2) + (floorHeight / 2);
        floor.transform.position = pos;

        spawner.active = false;
        Time.timeScale = 0;
        timeBest = PlayerPrefs.GetFloat("TimeBest");

        continueText.text = "PRESS ANY BUTTON TO START";
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStarted && Time.timeScale == 0)
        {
            if (Input.anyKeyDown)
            {
                timeManager.ManipulateTime(1f, 1f);
                ResetGame();
            }
        }

        if (!gameStarted)
        {
            blinkTime++;

            if (blinkTime % 40 == 0)
            {
                blink = !blink;

                continueText.canvasRenderer.SetAlpha(blink ? 0 : 1);
            }
            if (personalRecord)
                scoreText.text = TextTimeElapsed + "\n<color=#FF0>" + TextTimeBest + "</color>";
            else
                scoreText.text = TextTimeElapsed + '\n' + TextTimeBest;
        }
        else
        {
            timeElapsed += Time.deltaTime;
            scoreText.text = TextTimeElapsed;
        }
    }

    void OnPlayerKilled()
    {
        gameStarted = false;
        spawner.active = false;

        // remove the link
        var playerDestroyScript = player.GetComponent<DestroyOffscreen>();
        playerDestroyScript.DestroyCallback -= OnPlayerKilled;

        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        timeManager.ManipulateTime(0f, 5.5f);

        continueText.text = "PRESS ANY BUTTON TO RESTART";

        if (Mathf.Floor(timeElapsed) > Mathf.Floor(timeBest))
        {
            timeBest = timeElapsed;
            personalRecord = true;
            PlayerPrefs.SetFloat("TimeBest", timeBest);
        }
    }

    private void ResetGame()
    {
        gameStarted = true;
        spawner.active = true;
        timeElapsed = 0f;
        personalRecord = false;

        player = GameObjectUtil.Instantiate(playerPrefab, new Vector3(0, (Screen.height / PixelPerfectCamera.pixelsToUnits) / 2 + 100, 0));

        var playerDestroyScript = player.GetComponent<DestroyOffscreen>();
        playerDestroyScript.DestroyCallback += OnPlayerKilled;

        continueText.canvasRenderer.SetAlpha(0);
    }

    private string FormatTime(float value)
    {
        TimeSpan t = TimeSpan.FromSeconds(value);
        return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
    }
}
